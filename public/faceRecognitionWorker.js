const DEFAULT_MODELS_PATH = '/models';

// Загружаем патч который позволяет запускать face-api.js в воркере.
importScripts('/faceEnvWorkerPatch.js');
// Загружаем face-api.js библиотеку локально
importScripts('/face-api.js');

let areModelsLoaded = false;
let faceMatcher = null;
let faceMatcherDistance = null;

const loadModels = async (modelsPath = DEFAULT_MODELS_PATH) => {
  try {
    await faceapi.nets.tinyFaceDetector.loadFromUri(modelsPath);
    await faceapi.nets.faceLandmark68Net.loadFromUri(modelsPath);
    await faceapi.nets.faceRecognitionNet.loadFromUri(modelsPath);
    await faceapi.nets.faceExpressionNet.loadFromUri(modelsPath);
    await faceapi.nets.ssdMobilenetv1.loadFromUri(modelsPath);
    await faceapi.nets.ageGenderNet.loadFromUri(modelsPath);

    console.log('Models are loaded');
    areModelsLoaded = true;
  } catch (error) {
    console.log('Models loading error:', error);
    postMessage({ type: 'initError', error: error.message });
  }
};

function imageDataToHTMLCanvasElement(imageData) {
  const canvas = new OffscreenCanvas(imageData.width, imageData.height);
  const context = canvas.getContext('2d',{ willReadFrequently: true });
  context.putImageData(imageData, 0, 0);

  return canvas;
}

async function compareFaces(canvas, faceMatcher) {
  const newDetections = await faceapi.detectSingleFace(canvas).withFaceLandmarks().withFaceDescriptor();
  
  if (!newDetections) {
    return;
  }

  const bestMatch = faceMatcher.findBestMatch(newDetections.descriptor);

  if (bestMatch.distance < faceMatcherDistance) {
    const dataUrl = await getDataWithDetections(canvas, newDetections);

    postMessage({ type: 'result', dataUrl });
  }
};

async function getDataWithDetections(canvas, newDetections) {
  const ctx = canvas.getContext('2d', { willReadFrequently: true });
  ctx.drawImage(canvas, 0, 0, canvas.width, canvas.height);

  faceapi.draw.drawFaceLandmarks(canvas, newDetections);

  const blob = await canvas[
    canvas.convertToBlob 
      ? 'convertToBlob' // specs
      : 'toBlob'        // current Firefox
  ]();

  return new FileReaderSync().readAsDataURL(blob);
}

async function createFaceMatcher(imageData) {
  const canvas = imageDataToHTMLCanvasElement(imageData);

  const result = await faceapi
    .detectSingleFace(canvas)
    .withFaceLandmarks()
    .withFaceDescriptor();

  if (!result) {
    const error = 'Loaded Image is not recognized!!!';
    console.warn(error);
    throw new Error(error);
  }

  console.log('FaceMatcher is Created!!!');
  const faceMatcher = new faceapi.FaceMatcher(result);

  return faceMatcher;
}

function checkIsWorkerReadyForProcessing(areModelsLoaded, faceMatcher, faceMatcherDistance) {
  if (!areModelsLoaded) {
    const error = 'Модели не загружены!!!';
    console.warn(error);
    postMessage({ type: 'processError' , error });

    return false;
  }

  if (!faceMatcher) {
    const error = 'FaceMatcher не проинициализирован!!!';
    console.warn(error);
    postMessage({ type: 'processError' , error });

    return false;
  }

  if (!faceMatcherDistance) {
    const error = 'FaceMatcherDistance не проинициализирован!!!';
    console.warn(error);
    postMessage({ type: 'processError' , error });

    return false;
  }

  return true;
}

onmessage = async function (event) {
  const { type, photoData, videoFragmentData, distance, modelsPath } = event.data;

  if (type === 'init') {
    try {
      await loadModels(modelsPath);
      faceMatcher = await createFaceMatcher(photoData);
      faceMatcherDistance = distance;
      postMessage({ type: 'initSuccess' });
    } catch (error) {
      postMessage({ type: 'initError' , error: error.message});
    }

    return;
  }

  if (type === 'process') {
    const isWorkerReadyForProcessing = checkIsWorkerReadyForProcessing(areModelsLoaded, faceMatcher, faceMatcherDistance);

    if (isWorkerReadyForProcessing) {
      const canvasOfVideoFragment = imageDataToHTMLCanvasElement(videoFragmentData);
      compareFaces(canvasOfVideoFragment, faceMatcher);
    }
  }
};

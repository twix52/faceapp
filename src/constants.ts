export const RECOGNITION_INTERVAL = 100; // Частота рапознования лица на видео
export const BEST_MATCH_DISTANCE = 0.5; // Чувствительность распознования в %

export enum RecognitionSystemStatus {
    Success,
    Pending,
    Error,
    Unknown
};

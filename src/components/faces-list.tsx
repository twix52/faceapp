import { useEffect, useRef } from 'react';
import './faces-list.css';

type FaceListProps = {
  cleanPhotos: () => void;
  facesList: string[];
};

export const FacesList = ({ facesList, cleanPhotos }: FaceListProps) => {
  const endLineRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (endLineRef?.current && facesList.length) {
      endLineRef.current.scrollIntoView();
    }
  }, [endLineRef, facesList.length])


  return (
    <div className='faces'>
      <button onClick={cleanPhotos}>Clean Photos</button>
      <h1 className="header">{`Saved screenshots (${facesList.length})`}</h1>
      {!!facesList.length && 
       <div className='faces-block'>
          {facesList.map((face, index) => (
            <img
              alt={'saved item'}
              key={index}
              className="saved-image"
              src={face} />
          ))}
          <div ref={endLineRef}></div>
      </div>
      }
    </div>
  )
}

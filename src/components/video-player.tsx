import { LegacyRef, useRef } from 'react';
import './video-player.css';

const VIDEO_PALYER_HEIGHT = 200;
const VIDEO_PLAYER_WIDTH = 450;

type VideoPlayerProps = {
  onPlay: () => void;
  videoSrc: string;
  videoType: string;
  onEnded: () => void;
  onPause: () => void;
  forwardedRef: React.Ref<HTMLVideoElement>;
}

export const VideoPlayer: React.FC<VideoPlayerProps> = ({
  forwardedRef,
  onPlay,
  videoSrc,
  videoType,
  onEnded,
  onPause
}: VideoPlayerProps) => {
 
  return (
      <div className="player">
        <video
          ref={forwardedRef}
          width={VIDEO_PLAYER_WIDTH}
          height={VIDEO_PALYER_HEIGHT}
          controls
          autoPlay={true}
          onPlay={onPlay}
          onPause={onPause}
          onEnded={onEnded}
        >
          <source src={videoSrc} type={videoType} />
          Your browser does not support the video tag.
        </video>
      </div>
    );
}

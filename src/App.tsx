import './App.css';
import { VideoPlayer } from './components/video-player';
import { ChangeEvent, useEffect, useRef, useState } from 'react';
import { FacesList } from './components/faces-list';
import { BEST_MATCH_DISTANCE, RECOGNITION_INTERVAL, RecognitionSystemStatus } from './constants';

const facesVideoSrc = '/students.mp4';

const worker = new Worker('/faceRecognitionWorker.js');

export const App = () => {
  const [savedFaces, setSavedFaces] = useState<string[]>([]);
  const [isComparingImageLoaded, setIsComparingImageLoaded] = useState(false);
  const [uploadedImageSrc, setUploadedImageSrc] = useState<string>('');
  const [recognitionIntervalId, setRecognitionIntervalId] = useState<NodeJS.Timeout | null>(null);
  const [recognitionSystemStatus, setRecognitionSystemStatus] = useState(RecognitionSystemStatus.Unknown);
  const [recognitionSystemError, setRecognitionSystemError] = useState('');

  const inputRef = useRef<HTMLImageElement | null>(null);
  const videoRef = useRef<HTMLVideoElement | null>(null);

  const handlePlay = () => {
    if (!videoRef?.current) {
      const error = 'Не найден видеоплеер';
      console.warn(error);
      setRecognitionSystemStatus(RecognitionSystemStatus.Error);
      setRecognitionSystemError(error);
      return;
    }

    if (!inputRef?.current) {
      const error = 'Не найдена загруженная картинка';
      console.warn(error);
      setRecognitionSystemStatus(RecognitionSystemStatus.Error);
      setRecognitionSystemError(error);
      return;
    }

    const photoData = extractImageDataFromVideoOrImage(inputRef.current)

    const id = setInterval(() => {
      const videoFragmentData = extractImageDataFromVideoOrImage(videoRef.current);
      worker.postMessage({ type: 'process', photoData, videoFragmentData });
    }, RECOGNITION_INTERVAL);

    setRecognitionIntervalId(id);
  }

  const handleStopDetection = () => {
    if (recognitionIntervalId) {
      clearInterval(recognitionIntervalId);
      setRecognitionIntervalId(null);
      setRecognitionSystemError('');
    }
  }

  const handleLoadImage = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.files) {
      const imageSrc = URL.createObjectURL(event.target.files[0]);
      setUploadedImageSrc(imageSrc);
    } else {
      console.warn('Не удалось загрузить картинку')
    }
  }

  const handleCleanSavedPhotos = () => {
    setSavedFaces([]);
  }

  const extractImageDataFromVideoOrImage = (element: HTMLVideoElement | HTMLImageElement | null) => {
    if (!element) {
      return null;
    }

    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');

    if( element instanceof HTMLVideoElement) {
       // Установка размеров канваса равными размерам видео
      canvas.width = element.videoWidth
      canvas.height = element.videoHeight
    } else {
      // Установка размеров канваса равными размерам картинки
      canvas.width = element.width;
      canvas.height = element.height;
    }

    if (context) {
      // Отрисовка текущего кадра видео на канвасе
      context.drawImage(element, 0, 0, canvas.width, canvas.height);
        
      // Получение изображения в формате ImageData
      const imageData = context.getImageData(0, 0, canvas.width, canvas.height);

      return imageData;
    }
  }

  useEffect(() => {
    if (isComparingImageLoaded && uploadedImageSrc) {
      const photoData = extractImageDataFromVideoOrImage(inputRef.current);
      worker.postMessage({ type: 'init', photoData, distance: BEST_MATCH_DISTANCE });
      setRecognitionSystemStatus(RecognitionSystemStatus.Pending);
    }
  }, [uploadedImageSrc, isComparingImageLoaded]);


  useEffect(() => {
    if (recognitionSystemError) {
      handleStopDetection();
    }

  }, [recognitionSystemError]);


  useEffect(() => {
      worker.onmessage = function(event) {
        const { type, dataUrl, error } = event.data;

        if (type === 'initSuccess') {
          setRecognitionSystemStatus(RecognitionSystemStatus.Success);
        }

        if (type === 'initError') {
          setRecognitionSystemStatus(RecognitionSystemStatus.Error);
          setRecognitionSystemError(error);
        }

        if (type === 'processError') {
          setRecognitionSystemStatus(RecognitionSystemStatus.Error);
          setRecognitionSystemError(error);
        }

        if (type === 'result' && dataUrl) {
          setSavedFaces(prevData => [...prevData, dataUrl]);
        }
      };

      return () => {
        handleStopDetection();
        worker.terminate();
      }

  }, []);

  return (
      <div className="App">
        <div className="input-files">
          <input type="file" accept=".png" onChange={handleLoadImage}></input>
          {uploadedImageSrc &&
            <img
              alt="uploaded"
              onLoad={() => setIsComparingImageLoaded(true)}
              ref={inputRef}
              src={uploadedImageSrc}
            />}
        </div>
        {recognitionSystemStatus === RecognitionSystemStatus.Pending &&
          <div>
            <h2>Инициализация системы раcпознования...</h2>
          </div>
        }
        {recognitionSystemStatus === RecognitionSystemStatus.Error &&
          <div>
            <h2>Ошибка инициализации системы распознования</h2>
            <div>{recognitionSystemError}</div>
          </div>
        }
        {recognitionSystemStatus === RecognitionSystemStatus.Success &&
          <VideoPlayer
            forwardedRef={videoRef}
            videoSrc={facesVideoSrc}
            videoType={"video/mp4"}
            onPlay={handlePlay}
            onPause={handleStopDetection}
            onEnded={handleStopDetection}
          />
        }
        <FacesList cleanPhotos={handleCleanSavedPhotos} facesList={savedFaces} />
      </div>
    );
}
